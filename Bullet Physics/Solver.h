#pragma once
#include "Vector.h"
class Solver
{
public:
	virtual void Solve(std::Vector<Collision>& collisions,
		float dt) = 0;
};

