#pragma once
#include "Vector.h"
class Collider

struct CollisionPoints {
	Vector A;
	Vector B;
	Vector Normal;
	float Depth;
	bool HasCollision;
};

struct Transform {
	Vector Position;
	Vector Scale;
	quaternion Rotation;

};

struct Collider {
	virtual CollisionPoints TestCollision(
		const Transform* transform,
		const Collider* collider,
		const Transform* colliderTransform) const = 0;

	virtual CollisionPoints TestCollision(
		const Transform* transform,
		const BoxCollider* box,
		const Transform* boxTransform) const = 0;
};

struct BoxCollider : Collider {
	Vector Box;
	float Distance;

		CollisionPoints TestCollision(
			const Transform* transform,
			const Collider* collider,
			const Transform* colliderTransform) const override {}

		CollisionPoints TestCollision(
			const Transform* transform,
			const BoxCollider* box,
			const Transform* boxTransform) const override
};