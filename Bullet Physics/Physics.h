#pragma once
#include "Vector.h"
#include "Collider.h"
#include "Solver.h"

class Physics {
private:
	std::Vector<Object*> m_objects;	
	std::Vector<Solver*> m_solvers;
	Vector m_gravity = Vector()

public:
	void AddObject(Object* object)
	{
		m_objects.push_back(object);
	}
	void RemoveObject(Object* object)
	{
		if (!object) return;
		auto itr = std::find(m_objects.begin(), m_objects.end(), object);
		if (itr == m_objects.end()) return;
		m_objects.erase(itr);
	}

	void AddSolver (Solver* solver)
	void RemoveSolver (Solver* solver)

	void Step(
		float dt)
	{
		ResolveCollisions(dt);

		for (Object* obj : m_objects) {
			obj->Force += obj->Mass * m_gravity;
			obj->Velocity += obj->Force / obj->Mass * dt;
			obj->Position += obj->Velocity * dt;
			obj->Force = Vector(0, 0, 0);
		}
	}

	void ResolveCollisions(
		float dt)
	{
		std::Vector<Collision> collisions;
		for (Object* a : m_objects) {
			for (Object* b : m_objects) {
				if (a == b) break;

				if (!a->Collider || !b->Collider) continue;
				
				CollisionPoints points = a->Collider->TestCollision(a->Transform, b->Collider, b->Transform);

				if (points.HasCollision) {
					collisions.emplace_back(a, b, points);

			for (Solver* solver : m_solvers) {
				solver->Solve(collisions, dt);
	}

};
{
};

